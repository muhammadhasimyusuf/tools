import React, { useState } from 'react'
import { Layout, Menu, Button, theme } from 'antd';
import { AiOutlineMenuFold, AiOutlineMenuUnfold, AiOutlineUser } from "react-icons/ai";
import bgimage from '../../assets/Img/ptkmks.jpg'

const { Header, Sider, Content } = Layout;

function Cointainer({children}) {

    const [collapsed, setCollapsed] = useState(false);
    const {
      token: { colorBgContainer, borderRadiusLG },
    } = theme.useToken();
  
    return (
      <Layout
        className='max-h-screen h-screen'
        style={{ position: 'relative', overflow: 'hidden' }}
      >
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundImage: `url(${bgimage})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            filter: 'blur(8px)',
            // zIndex: -1,
          }}
        >
          
        </div>
        <Content
          style={{
            margin: '9% 10%',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
            position: 'relative',
            boxShadow : 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
            zIndex: 1,
          }}
        >
          {children}
        </Content>
      </Layout>
    )
}

export default Cointainer

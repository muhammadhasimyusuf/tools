import React from "react";
import Each from "../../Component/Each";
import kristal from "../../assets/Img/Osilator/kristal.png";
import kristalrangkaian from "../../assets/Img/Osilator/kristalrangkaian.png";
import module from "../../assets/Img/Osilator/module.png";
import Step16 from "../../assets/Img/Osilator/3 Fault Simulator TP 1,2,3,4,5.jpg";
import Step18 from "../../assets/Img/Osilator/Osilator Kristal Fault Simulator 3 TP 5.jpg";
import Step12 from "../../assets/Img/Osilator/TP 1 SAMPAI TP 5.jpg";
import Step10tp1 from "../../assets/Img/Osilator/XTAL TP1.jpg";
import Step10tp3 from "../../assets/Img/Osilator/XTALTP3.jpg";
import Step10tp2 from "../../assets/Img/Osilator/XTAL TP2.jpg";
import Step10tp4 from "../../assets/Img/Osilator/XTAL TP4.jpg";
import Step10tp5 from "../../assets/Img/Osilator/XTAL TP5.jpg";
import { Image } from "antd";

function Osilator() {
  return [
    {
      title: "Pendahuluan",
      content: (
        <div>
          <p>
            Salah satu bagian utama pemancar radio adalah rangkaian pembangkit
            frekuensi tinggi yang dikenal dengan istilah osilator. Frekuensi
            yang dibangkitkan berfungsi sebagai sumber frekuensi pembawa sinyal
            informasi. Pemilihan jenis osilator didasarkan pada kriteria:
            <ul>
              {
                <Each
                  of={[
                    "Frekuensi keluaran yang dinginkan.",
                    "Stabilitas frekuensi.",
                    "Frekuensi keluaran yang dapat bervariasi pada rentang tertentu (variabel) atau frekuensi keluaran yang bearnya tetap (fixed).",
                    "Distorsi bentuk gelombang yang diperbolehkan.",
                    "Daya keluaran yang diperlukan.",
                  ]}
                  render={(item, index) => (
                    <li key={index}>
                      <span className="px-2">{index + 1}</span> {item}
                    </li>
                  )}
                />
              }
            </ul>
            Rangkaian pembangkit frekuensi pada trainer ini menggunakan jenis
            osilator kristal (Xtal = kristal). Rangkaian ekivalen dan simbol
            skematik kristal dapat dilihat pada gambar berikut;
          </p>
          <div className="w-full flex justify-center">
            <Image src={kristal} />
          </div>
          <p>
            Dari rangkaian ekivalen tersebut dapat dijelaskan bahwa rangkaian
            ini sebagai rangkaian seri dengan rugi-rugi resistif yang diparalel
            dengan kapasitor. Frekuensi resonansi kedua rangkaian ini (seri dan
            paralel) sangat berdekatan, sehingga impedansi kristal bervariasi
            dan berimpit dalam rentang frekuensi yang sempit. Hal ini menandakan
            bahwa kristal merupakan rangkaian dengan nilai Q yang tinggi.
            Kestabilan frekuensi yang dihasilkan kristal ini juga dipengaruhi
            oleh bahan dasar kristal tersebut, yaitu dari QUARTZ. Bahan ini
            memiliki karakteristik kestabilan waktu dan suhu yang baik. Pada
            osilator kristal, frekuensi yang dibangkitkan bergantung pada besar
            frekuensi yang dihasilkan oleh kristal tersebut. Komponen aktif
            rangkaian osilator kristal ini dapat berbentuk tabung hampa,
            transistor, FET atau IC. Sedangkan osilator kristal pada trainer ini
            menggunakan komponen aktif berupa transistor yang disusun menjadi
            rangkaian jenis Colpitts yang praktis dan umum digunakan. Frekuensi
            kristal yang dipakai merupakan nilai yang sering dipakai pada
            rangkaian elektronika dan banyak di pasaran, yaitu 3597,545kHz.
          </p>
          <div className="w-full flex justify-center">
            <Image src={kristalrangkaian} />
          </div>
          <p>
            Pada percobaan ini akan dipelajari karakteristik penguat osilator
            yang menggunakan Kristal sebagai penentu besar frekuensi.
          </p>
        </div>
      ),
    },
    {
      title: "Step 1",
      content: (
        <div>
          <p>
            Pasanglah panel Power Supply dan Oscillator X'tal pada rel bingkai
            standar.
          </p>
        </div>
      ),
    },
    {
      title: "Step 2",
      content: (
        <div>
          <p>Hubungkan semua peralatan sesuai Gambar berikut:</p>
          <div className="w-full justify-center flex">
            <Image src={module} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 3",
      content: (
        <div>
          <p>
            Periksa kembali sambungan semua peralatan. Jika sudah benar,
            nyalakan catu-daya.
          </p>
        </div>
      ),
    },
    {
      title: "Step 4",
      content: (
        <div>
          <p>
            Dengan menggunakan osiloskop, amati bentuk gelombang keluaran,
            kemudian gambarkan bentuk gelombang tersebut.
          </p>
        </div>
      ),
    },
    {
      title: "Step 5",
      content: (
        <div>
          <p>Tentukan besar tegangan dengan menggunakan osiloskop.</p>
          <p>Vp-p = …………. Volt.</p>
        </div>
      ),
    },
    {
      title: "Step 6",
      content: (
        <div>
          <p>Tentukan besar frekuensi dengan menggunakan osiloskop.</p>
          <p>Fosc = …………..Hz = ……………… kHz.</p>
        </div>
      ),
    },
    {
      title: "Step 7",
      content: (
        <div>
          <p>Ukur frekuensinya dengan menggunakan frequency counter.</p>
          <p>Fosc = …………..Hz = ……………… kHz.</p>
        </div>
      ),
    },
    {
      title: "Step 8",
      content: (
        <div>
          <p>
            Lihatlah angka yang tercantum pada kristal yaitu: 3.579,545 kHz.
          </p>
        </div>
      ),
    },
    {
      title: "Step 9",
      content: (
        <div>
          <p>
            Bandingkan besar frekuensi berdasarkan perhitungan dengan osiloskop,
            pencacah frekuensi, dan berdasarkan angka yang tercantum pada
            kristal. Analisalah apabila terdapat perbedaan.
          </p>
        </div>
      ),
    },
    {
      title: "Step 10",
      content: (
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4
            dan TP5.
          </p>
          <span>
            TP1
            <Image src={Step10tp1}/>
          </span>
          <span>
            TP2
            <Image src={Step10tp2}/>
          </span>
          <span>
            TP3
            <Image src={Step10tp3}/>
          </span>
          <span>
            TP4
            <Image src={Step10tp4}/>
          </span>
          <span>
            TP5
            <Image src={Step10tp5}/>
          </span>
        </div>
      ),
    },
    {
      title: "Step 11",
      content: (
        <div>
          <p>
            Aktifkan fault simulator 1 panel osilator Kristal. Indikator “1”
            menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 12",
      content: (
        <div>
          <p>
            Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4
            dan TP5.
          </p>
          <Image src={Step12}/>
        </div>
      ),
    },
    {
      title: "Step 13",
      content: (
        <div>
          <p>
            Aktifkan fault simulator 2 osilator Kristal. Indikator “2” menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 14",
      content: (
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4,
            dan TP5.
          </p>
        </div>
      ),
    },
    {
      title: "Step 15",
      content: (
        <div>
          <p>
            Aktifkan fault simulator 3 osilator Kristal. Indikator “3” menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 16",
      content: (
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4,
            dan TP5.
          </p>
          <Image src={Step16} />
        </div>
      ),
    },
    {
      title: "Step 17",
      content: (
        <div>
          <p>
            Aktifkan ketiga fault simulator osilator Kristal yang ada. Indikator
            “1”, “2” dan “3” menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 18",
      content: (
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4
            dan TP5.
          </p>
          <Image src={Step18} />
        </div>
      ),
    },
    {
      title: "Kesimpulan",
      content: (
        <div>
          <p>
            <ul>
              <Each
                of={[
                  "Proses pada percobaan di atas adalah tahapan proses pengukuran dan pengamatan rangalan dasar suatu pemancar radio. Bagaimana dan apa penyebabnya bila suatu osilator tidak berfungsi dapat kita pelajari dari proses di atas.",
                  "Osilator kristal selalu mempunyai frekuensi dasar keluaran yang besarnya sesuai dengan nilai frekuensi kristalnya. Jadi, bila kita amati dengan penganalisis spektrum (spectrum analyzer), akan terdapat komponen-komponen harmonisa keluaran tersebut, yaitu kelipatan-kelipatan frekuensi kristal tersebut.",
                  "Harmonisa-harmonisa ini (yang beramplitudo besar) akan cukup mengganggu dan memboroskan energi yang di pancarkan di tingkatan selanjutnya.",
                ]}
                render={(item, index) => <li> <span className="px-2">{index + 1}</span> {item}</li>}
              />
            </ul>
          </p>
        </div>
      ),
    },
  ];
}

export default Osilator;

import React from "react";
import img from "../../assets/Img/Buffer/imagebuffer.png";
import module from "../../assets/Img/Buffer/module.png";
import masukkan from "../../assets/Img/Buffer/Masukan Buffer.jpg";
import keluaran from "../../assets/Img/Buffer/Keluaran Buffer.jpg";
import tp1_8 from "../../assets/Img/Buffer/8.Buffer TP 1.jpg";
import tp2_8 from "../../assets/Img/Buffer/8. Buffer TP 2.jpg";
import tp3_8 from "../../assets/Img/Buffer/8. Buffer TP 3.jpg";
import tp1_10 from "../../assets/Img/Buffer/10F123, TP (1).jpg"
import tp2_10 from "../../assets/Img/Buffer/10F123, TP (2).jpg";
import tp3_10 from "../../assets/Img/Buffer/10F123, TP (3).jpg";
import tp1_12 from "../../assets/Img/Buffer/12F123, TP  (1).jpg"
import tp2_12 from "../../assets/Img/Buffer/12F123, TP  (2).jpg";
import tp3_12 from "../../assets/Img/Buffer/12F123, TP  (3).jpg";
import tp1_16 from "../../assets/Img/Buffer/16F123, TP 1.jpg"
import tp2_16 from "../../assets/Img/Buffer/16F123, TP 2.jpg";
import tp3_16 from "../../assets/Img/Buffer/16F123, TP 3.jpg";
import tp1_14 from "../../assets/Img/Buffer/14TP1 FAULT SIMULATOR 3 (2).jpg"
import tp2_14 from "../../assets/Img/Buffer/14TP 2 F3.jpg";
import tp3_14 from "../../assets/Img/Buffer/14TP 3 F3.jpg";
import { Image } from "antd";

function Buffers() {
  return [
    {
      title: "Pendahuluan",
      content: (
        <div>
          Frekuensi radio (RF) yang dibangkitkan oleh osilator pada kenyataannya
          masin belum cukup memadai untuk dapat dipancarkan ke udara. Karena itu
          untuk masuk ke penguat daya, besar tegangannya harus disesuaikan oleh
          suatu rangkaian yang dikenal dengan nama penyangga. Sinyal RF keluaran
          rangkaian osilator kristal yang tidak begitu besar diteruskan melalui
          trafo kopling (penggandeng) T1 (pada osilator) sebagai masukan yang
          masuk ke rangkaian buffer. Sinyal kemudian diperkuat dan diteruskan
          melalui trafo kopling T2 sebagai keluaran. Besar sinyal RF yang
          diterima dari osilator sekitar 2V, pada rangkaian buffer sinyal ini
          distabilkan, kemudian keluarannya diresonansikan oleh kopling T1 dan
          diteruskan ke rangkaian selanjutnya, yaitu driver/power (daya).
          <div className="w-full flex justify-center">
            <Image src={img} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 1",
      content: (
        <div>
          <p>
            Pasang panel Power Supply, Oscillator 'tal dan Buffer pada rel
            bingkai standar.
          </p>
        </div>
      ),
    },
    {
      title: "Step 2",
      content: (
        <div>
          <p>Hubungkan peralatan sesuai dengan Gambar berikut:</p>
          <div className="w-full justify-center flex">
            <Image src={module} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 3",
      content: (
        <div>
          <p>
            Periksa kembali hubungan semua peralatan. Jika sudah benar, nyalakan
            catu-daya.
          </p>
        </div>
      ),
    },
    {
      title: "Step 4",
      content: (
        <div>
          <p>
            Dengan menggunakan osiloskop amati bentuk gelombang masukan dan
            gelombang keluaran.
          </p>
        </div>
      ),
    },
    {
      title: "Step 5",
      content: (
        <div>
          <p>
            Gambarlah bentuk gelombang masukan dan gelombang keluaran tersebut.
          </p>
          <p>Masukkan :</p>
          <div className="w-full flex justify-center">
          <Image src={masukkan} />
          </div>
          <p>Keluaran :</p>
          <Image src={keluaran} />
        </div>
      ),
    },
    {
      title: "Step 6",
      content: (
        <div>
          <p>
            Periksa besar tegangan gelombang masukan dan gelombang keluaran
            tersebut dengan menggunakan osiloskop.
          </p>
        </div>
      ),
    },
    {
      title: "Step 7",
      content: (
        <div>
          <p>
            Tentukan besar penguatan tegangan antara tegangan masukan dan
            tegangan keluaran.
          </p>
        </div>
      ),
    },
    {
      title: "Step 8",
      content: (
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2 dan TP3.
          </p>
          <br />
          <Image src={tp1_8} />
          <br />
          <Image src={tp2_8} />
          <br />
          <Image src={tp3_8} />
        </div>
      ),
    },
    {
      title: "Step 9",
      content: (
        <div>
          <p>
            Aktifkan fault simulator 1 panel buffer. Indikator "1" pada panel
            buffer menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 10",
      content: (
        <div>
          <p>
            Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2 dan TP3.
          </p>
          <br />
          <div>
            <p>TP1</p>
            <Image src={tp1_10} />
          </div>
          <div>
            <p>TP2</p>
            <Image src={tp2_10} />
          </div>
          <div>
            <p>TP3</p>
            <Image src={tp3_10} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 11",
      content: (
        <div>
          <p>
            Aktifkan fault simulator 2 panel buffer. Indikator "2" pada panel
            buffer menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 12",
      content: (
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2 dan TP3.
          </p>
          <br />
          <div>
            <p>TP1</p>
            <Image src={tp1_12} />
          </div>
          <div>
            <p>TP2</p>
            <Image src={tp2_12} />
          </div>
          <div>
            <p>TP3</p>
            <Image src={tp3_12} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 13",
      content: (
        <div>
          <p>
            Aktifkan fault simulator 3 panel buffer. Indikator "3" pada panel
            buffer menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 14",
      content: (
        <div>
          <p>
            Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2 dan TP3.
          </p>
          <br />
          <div>
            <p>TP1</p>
            <Image src={tp1_14} />
          </div>
          <div>
            <p>TP2</p>
            <Image src={tp2_14} />
          </div>
          <div>
            <p>TP3</p>
            <Image src={tp3_14} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 15",
      content: (
        <div>
          <p>
            Aktifkan ketiga fault simulator pada panel buffer. Indikator "1",
            "2", "3" pada panel buffer menyala.
          </p>
        </div>
      ),
    },
    {
      title: "Step 16",
      content: (
        <div>
          <p>
            Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2 dan TP3.
          </p>
          <br />
          <div>
            <p>TP1</p>
            <Image src={tp1_16} />
          </div>
          <div>
            <p>TP2</p>
            <Image src={tp2_16} />
          </div>
          <div>
            <p>TP3</p>
            <Image src={tp3_16} />
          </div>
        </div>
      ),
    },
    {
      title: "Kesimpulan",
      content: (
        <div>
          <p>
            Proses pada percobaan di atas adalah untuk mengukur dan mengamati
            proses penguatan suatu osilator agar osilator tersebut berfungsi
            sebagaimana seharusnya Juga dapat kita pelajari bahwa bila suatu
            penguat sinyal frekuensi tinggi mengalami gangguan, faktor-faktor
            penyebab gangguan ini bisa kita perkirakan dan kita lokalisasi.
          </p>
        </div>
      ),
    },
  ];
}

export default Buffers;

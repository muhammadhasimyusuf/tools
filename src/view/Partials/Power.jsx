import React from "react";
import Each from "../../Component/Each";
import kristal from "../../assets/Img/Osilator/kristal.png";
import kristalrangkaian from "../../assets/Img/Osilator/kristalrangkaian.png";
import module from "../../assets/Img/PSU/image.png";
import module1 from "../../assets/Img/PSU/image1.png";
import module3 from "../../assets/Img/PSU/image3.png";
import module4 from "../../assets/Img/PSU/image4.png";
import { Image } from "antd";

function PowerSupply() {
  return [
    {
      title: "Step 1",
      content: (
        <div>
          <p>Pasang panel Power supply pada rel bingkai standar.</p>
        </div>
      ),
    },
    {
      title: "Step 2",
      content: (
        <div>
          <p>Hubungkan steker masukan pada jala-jala PLN.</p>
          {/* <div className="w-full justify-center flex">
            <Image src={module} />
          </div> */}
        </div>
      ),
    },
    {
      title: "Step 3",
      content: (
        <div>
          <p>
            Hubungkan sema peralatan seperti pada Gambar berikut:
            <div className="w-full justify-center flex">
              <Image src={module} />
            </div>
          </p>
        </div>
      ),
    },
    {
      title: "Step 4",
      content: (
        <div>
          <p>Atur posisi hambatan geser pada posisi maksimum.</p>
        </div>
      ),
    },
    {
      title: "Step 5",
      content: (
        <div>
          <p>
            Nyalakan saklar catu-daya, kemudian perhatikan arus yang mengalir
            (pada ampere-meter) dan besarnya tegangan (pada osiloskop).
          </p>
          {/* <p>Vp-p = …………. Volt.</p> */}
        </div>
      ),
    },
    {
      title: "Step 6",
      content: (
        <div>
          <p>Atur hambatan geser agar arus yang mengalir besarnya 0,5A.</p>
          {/* <p>Fosc = …………..Hz = ……………… kHz.</p> */}
        </div>
      ),
    },
    {
      title: "Step 7",
      content: (
        <div>
          <p>
            . Perhatikan besarnya tegangan pasok (supply), serta perhatikan
            grafik yang ditampilkan pada layar osiloskop
          </p>
          <b className="text-center flex flex-col justify-center">
            <p>
              I = 0,5 ampere. <br />V pasok = ....volt
            </p>
            <Image
              className="flex justify-center"
              src={module1}
            />
          </b>
        </div>
      ),
    },
    {
      title: "Step 8",
      content: (
        <div>
          <p>
          Ubah hambatan geser agar arus yang mengalir menjadi 2A. Besar arus ini adalah besar arus yang mendekati besar arus maksimum catu-daya (2.5A)
          </p>
          <b className="text-center flex flex-col justify-center">
            <p>
              I = 2 ampere. <br />V pasok = ....volt
            </p>
            <Image
              className="flex justify-center"
              src={module3}
            />
          </b>
        </div>
      ),
    },
    {
      title: "Step 9",
      content: (
        <div>
          <p>
          Sesuaikah besar kedua tegangan tersebut dengan label yang tercantum pada panel? Jika tidak, berikan alasannya.
          </p>
        </div>
      ),
    },
    {
      title: "Step 10",
      content: (
        <div>
          <p>
          Ubah hambatan geser agar arus yang mengalir menjadi 2A.
          </p>
        </div>
      ),
    },
    {
      title: "Step 11",
      content: (
        <div>
          <p>
          Dengan merubah posis kedudukan pengukuran tegangan AC pada osiloskop dan dengan V/div yang lebih keil, amati tegangan DC keluarannya, ada ripple atau tidak. Seandainya ada, ukurlah tegangan ripple-nya dan hitung faktor ripple-nya.
          </p>
          <b>
            <p>
            I                             = 2.3 ampere
            </p>
            <p>
            V pasok	   = ………...volt
            </p>
            <p>
            V ripple	   = …………volt
            </p>
            <p>
            Faktor rippleriak = ……………%
            </p>
          </b>
          <center>
            <Image
              src={module4}
            />
          </center>
        </div>
      ),
    },
    {
      title: "Step 12",
      content: (
        <div>
          <p>
          Perhatikan tegangan pasokan. Apakah besarnya ada perubahan atau tetap. Perhatikan juga layar pada osiloskop. Mulus, atau bergelombangkah grafiknya?
          </p>
        </div>
      ),
    },
    {
      title: "Step 13",
      content: (
        <div>
          <p>
          Matikan catu-daya.
          </p>
        </div>
      ),
    },
    {
      title: "Kesimpulan",
      content: (
        <div>
          <p>
          Catu-daya yang baik akan mampu mengeluarkan arus yang cukup sesuai kemampuan (rating) yang dirancang oleh pabrik. Pada arus mendekati maksimum, bila diukur dengan voltmeter, tegangan keluaran catu-daya akan sama dengan tegangan pada saat arus baru 0,5A. Hal ini menunjukkan bahwa catu-daya tersebut mash mampu mengeluarkan arus sebesar nilai tersebut. Bila keluaran catu-daya tersebut diperiksa dengan osiloskop, akan terlihat bahwa pada saat 0,5A, grafik pada osiloskop akan mulus dan pada arus keluaran mendekati maksimum akan mulai tidak mulus (akan ada ripple).
          </p>
        </div>
      ),
    },
  ];
}

export default PowerSupply;

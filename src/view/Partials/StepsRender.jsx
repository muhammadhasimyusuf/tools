import { Popover, Steps, theme } from "antd";
import React from "react";
import { useSelector } from "react-redux";

function StepsRender() {
  const step = useSelector((state) => state.StepSlice);
  const { token } = theme.useToken();
  const contentStyle = {
    // lineHeight: '100%',
    textAlign: "left",
    color: token.colorTextTertiary,
    backgroundColor: token.colorFillAlter,
    borderRadius: token.borderRadiusLG,
    border: `1px dashed ${token.colorBorder}`,
    marginTop: 16,
  };
  // const index = step.data.findIndex()
  return (
    <>
     <div className="overflow-x-hidden">
     <Steps 
      // progressDot
      progressDot={ (dot, { status, index }) => {
        // if(status === 'process'){
          return <Popover
        content={
          <span>
            step {index} status: {status}
          </span>
        }
      >
        {dot}
      </Popover>
      // }
    }}
      size="small"
      current={step.current} 
      items={step.data.filter((data, i) => i + 7 >= step.current)}
      />
     </div>
      <div className="min-h-[83%] " style={contentStyle}>
        {step.data[step.current].content}
      </div>
    </>
  );
}

export default StepsRender;

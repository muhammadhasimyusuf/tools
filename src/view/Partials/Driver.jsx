import React from "react";
import img from "../../assets/Img/Driver/image.png";
import module from "../../assets/Img/Driver/module.png";
import { Image } from "antd";

function Driver() {
  return [
    {
      title: "Pendahuluan",
      content: (
        <div>
          <p>
            Keluaran buffer pada kenyataannya masih belum mampu memancarkan
            frekuensi radio (RF). Untuk itu, sebagai lanjutan dari penguat
            buffer diperlukan sebuah penguat daya lagi sebagai tahapan akhir
            untuk frekuensi pembawa (carrier) maupun sinyal informasi untuk
            dipancarkan. Keluaran sinyal RF yang dihasilkan dari rangkaian
            buffer diteruskan masuk ke masukan rangkaian driver-power RF melalui
            kopling kapasitor (Gambar 4.1). Rangkaian driver-power RF ini
            merupakan rangkaian yang umum dan banyak digunakan untuk pemancar
            transistor yang berdaya tidak terlalu besar.
          </p>
          <br />
          <div className="w-full justify-center flex">
            <Image className=" self-center" src={img} />
          </div>
          <p>
            Sinyal yang masuk melalui terminal masukan, dikopling C1 akan
            dikuatkan oleh transistor TR1, sehingga cukup kuat untuk
            mengaktifkan TR2 agar menghasilkan daya sekitar 7 watt. Sinyal RF
            hasil penguatan TR2 tersebut masih mengandung harmonisa-harmonisa
            gelombang dan sinyal-sinyal gelombang yang tidak dikehendaki. Oleh
            karena itu keluaran TR2 tersebut dilewatkan melalui rangkaian filter
            yang mempunyai karakteristik meneruskan frekuensi di bawah freuensi
            cut-off sinyal yang masuk (ow pass filter/LPF). Dalam rangkaian ini
            frekuensi cut-off nya sekitar 4MHz. Hasil proses filter ini,
            harmonisa dan gelombang frekuensi (yang tidak dikehendaki) akan
            mempunyai amplitudo yang kecil sekali atau bahkan tidak ada sama
            sekali. Pada percobaan ini dipelajari proses kerja penguat daya yang
            sesuai dengan kebutuhan rangkaian sistem pemancar pada trainer ini.
          </p>
        </div>
      ),
    },
    {
      title: "Step 1",
      content: [
        <div>
          <p>
            Pasang panel Power Supply, Oscillator Xtal, Buffer dan Driver/Power
            RF pada rel bingkai standar.
          </p>
        </div>,
      ],
    },
    {
      title: "Step 2",
      content: [
        <div>
          <p> Hubungkan dummy load pada keluaran power RF.</p>
        </div>,
      ],
    },
    {
      title: "Step 3",
      content: [
        <div>
          <p> Hubungkan peralatan sesuai Gambar berikut</p>
          <div className="w-full justify-center flex">
            <Image src={module} />
          </div>
        </div>,
      ],
    },
    {
      title: "Step 4",
      content: <div>
          <p>
            Periksa kembali sambungan semua peralatan. Jika sudah benar,
            nyalakan catu-daya.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      
    },
    {
      title: "Step 5",
      content: [
        <div>
          <p>
            Dengan menggunakan osiloskop amati bentuk gelombang masukan dan
            gelombang keluaran.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 6",
      content: [
        <div>
          <p>
            Gambarlah bentuk gelombang masukan dan gelombang keluaran tersebut.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 7",
      content: [
        <div>
          <p>
            Tentukan besar tegangan gelombang masukan dan gelombang keluaran
            tersebut dengan menggunakan osiloskop.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 8",
      content: [
        <div>
          <p>
            Tentukan besar penguatan tegangan antara tegangan masukan dan
            tegangan keluaran.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 9",
      content: [
        <div>
          <p>
            Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3 dan
            beban pengganti antena (dummy load).
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 10",
      content: [
        <div>
          <p>
            Aktifkan fault simulator 1 panel power RF, indikator "1" pada panel
            RF menyala.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 11",
      content: [
        <div>
          <p>
            Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2, TP3 dan
            dummy load
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 12",
      content: [
        <div>
          <p>
            Aktifkan fault simulator 2 panel power RF. Indikator "2" pada panel
            RF jadi menyala.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 13",
      content: [
        <div>
          <p>
            Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2, TP3 dan
            dummy load.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
    {
      title: "Step 14",
      content: [
        <div>
          <p>
            Aktifkan fault simulator 3 panel Power RF. Indikator "3" pada panel
            jadi menyala.
          </p>
          <div className="w-full justify-center flex">
            {/* <Image src={module} /> */}
          </div>
        </div>,
      ],
    },
  ];
}

export default Driver;

import React from 'react'

function Frequency() {
  return [
          
    {
      title: '',
      content: <div>
        Pasang panel Power supply pada rel bingkai standar
      </div>,
    },
    {
      title: '',
      content: 'Hubungkan steker masukan pada jala-jala PLN',
    },
    {
      title: '',
      content: 'Hubungkan pada peralatan',
    },
    {
      title: '',
      content: 'Atur posisi hambatan geser pada posisi maksimum',
    },
    {
      title: '',
      content: 'Nyalakan saklar catu-daya, kemudian perhatikan arus yang mengalir (pada ampere-meter) dan besarnya tegangan (pada osiloskop)',
    },
    {
      title: '',
      content: 'Atur hambatan geser agar arus yang mengalir besarnya 0,5A',
    },
    {
      title: '',
      content: 'Perhatikan besarnya tegangan pasok (supply), serta perhatikan grafik yang ditampilkan pada layar osiloskop',
    },
    {
      title: '',
      content: 'Ubah hambatan geser agar arus yang mengalir menjadi 2A. Besar arus ini adalah besar arus yang mendekati besar arus maksimum catu-daya (2.5A)',
    },
    {
      title: '',
      content: 'Sesuaikah besar kedua tegangan tersebut dengan label yang tercantum pada panel? Jika tidak, berikan alasannya',
    },
    {
      title: '',
      content: 'Ubah hambatan geser agar arus yang mengalir menjadi 2A',
    },
    {
      title: '',
      content: [
        <div className=''> 
          Dengan merubah posis kedudukan pengukuran tegangan AC pada osiloskop dan dengan V/div yang lebih keil, amati tegangan DC keluarannya, ada ripple atau tidak. Seandainya ada, ukurlah tegangan ripple-nya dan hitung faktor ripple-nya.
         </div>
      ],
    },
    {
      title: '',
      content: '. Perhatikan tegangan pasokan. Apakah besarnya ada perubahan atau tetap. Perhatikan juga layar pada osiloskop. Mulus, atau bergelombangkah grafiknya',
    },
    {
      title: '',
      content: '. Perhatikan tegangan pasokan. Apakah besarnya ada perubahan atau tetap. Perhatikan juga layar pada osiloskop. Mulus, atau bergelombangkah grafiknya',
    },
  
 ]
}

export default Frequency
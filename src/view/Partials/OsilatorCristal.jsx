import React from "react";
import Each from "../../Component/Each";
import kristal from "../../assets/Img/Osilator/kristal.png";
import kristalrangkaian from "../../assets/Img/Osilator/kristalrangkaian.png";
import module from "../../assets/Img/Osilator/module.png";
import { Image } from "antd";

function OsilatorCristal() {
  return [
    {
      title: "Step 1",
      content: (
        <div>
          <p>Pasanglah panel Power Supply dan Oscillator X'tal pada rel bingkai standar.</p>
        </div>
      ),
    },
    {
      title: "Step 2",
      content: (
        <div>
          <p>Hubungkan semua peralatan sesuai Gambar 2.3 berikut:</p>
          <div className="w-full justify-center flex">
            <Image src={module} />
          </div>
        </div>
      ),
    },
    {
      title: "Step 3",
      content: (
        <div>
          <p>Periksa kembali sambungan semua peralatan. Jika sudah benar, nyalakan catu-daya.</p>
        </div>
      ),
    },
    {
      title: "Step 4",
      content: (
        <div>
          <p>Dengan menggunakan osiloskop amati bentuk gelombang keluaran, kemudian gambarkan bentuk gelombang tersebut.</p>
        </div>
      ),
    },
    {
      title: "Step 5",
      content: (
        <div>
          <p>Tentukan besar tegangan dengan menggunakan osiloskop.</p>
          <p>Vp-p = …………. Volt.</p>
        </div>
      ),
    },
    {
      title: "Step 6",
      content: (
        <div>
          <p>Tentukan besar frekuensi dengan menggunakan osiloskop.</p>
          <p>Fosc = …………..Hz = ……………… kHz.</p>
        </div>
      ),
    },
    {
      title: "Step 7",
      content: (
        <div>
          <p>Ukur frekuensinya dengan menggunakan frequency counter.</p>
          <p>Fosc = …………..Hz = ……………… kHz.</p>
        </div>
      ),
    },
    {
      title: "Step 8",
      content: (
        <div>
          <p>Lihatlah angka yang tercantum pada kristal yaitu: 3.579,545 kHz.</p>
        </div>
      ),
    },
    {
      title: "Step 9",
      content: (
        <div>
          <p>Bandingkan besar frekuensi berdasarkan perhitungan dengan osiloskop, pencacah frekuensi, dan berdasarkan angka yang tercantum pada kristal. Analisalah apabila terdapat perbedaan.</p>
        </div>
      ),
    },
    {
      title: "Step 10",
      content: (
        <div>
          <p>Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4, dan TP5.</p>
        </div>
      ),
    },
    {
      title: "Step 11",
      content: (
        <div>
          <p>Aktifkan fault simulator 1 panel osilator Kristal. Indikator "1" menyala.</p>
        </div>
      ),
    },
    {
      title: "Step 12",
      content: (
        <div>
          <p>Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4, dan TP5.</p>
        </div>
      ),
    },
    {
      title: "Step 13",
      content: (
        <div>
          <p>Aktifkan fault simulator 2 osilator Kristal. Indikator "2" menyala.</p>
        </div>
      ),
    },
    {
      title: "Step 14",
      content: (
        <div>
          <p>Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4, dan TP5.</p>
        </div>
      ),
    },
    {
      title: "Step 15",
      content: (
        <div>
          <p>Amati fault simulator 3 osilator Kristal. Indikator "3" menyala.</p>
        </div>
      ),
    },
    {
      title: "Step 16",
      content: (
        <div>
          <p>Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4, dan TP5.</p>
        </div>
      ),
    },
    {
      title: "Step 17",
      content: (
        <div>
          <p>Aktifkan ketiga fault simulator osilator Kristal yang ada. Indikator "1", "2", dan "3" menyala.</p>
        </div>
      ),
    },
    {
      title: "Step 18",
      content: (
        <div>
          <p>Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, TP4, dan TP5.</p>
        </div>
      ),
    },
    {
      title: "Kesimpulan",
      content: (
        <div>
          <p>
            1. Proses pada percobaan di atas adalah tahapan proses pengukuran dan pengamatan rangkaian dasar suatu pemancar radio. Bagaimana dan apa penyebabnya bila suatu osilator tidak berfungsi dapat kita pelajari dari proses di atas.
          </p>
          <p>
            2. Osilator kristal selalu mempunyai frekuensi dasar keluaran yang besarnya sesuai dengan nilai frekuensi kristalnya. Jadi, bila kita amati dengan penganalisis spektrum (spectrum analyzer), akan terdapat komponen-komponen harmonisa keluaran tersebut, yaitu kelipatan-kelipatan frekuensi kristal tersebut.
          </p>
          <p>
            3. Harmonisa-harmonisa ini (yang beramplitudo besar) akan cukup mengganggu dan memboroskan energi yang dipancarkan di tingkatan selanjutnya.
          </p>
        </div>
      ),
    },
  ];
  
}

export default OsilatorCristal;

import { createSlice } from "@reduxjs/toolkit";

const StepSlice = createSlice({
    name: "Step",
    initialState: {
        data : [],
        current : 0,
     },
    reducers: {
       setDataStep(state, { payload }){
          state.data = payload
       },
       changeCurrent(state, { payload }){
          state.current = payload
       },
    },
});

export const { setDataStep, changeCurrent } = StepSlice.actions;
export default StepSlice.reducer;
import React from "react";
import Each from "../../Component/Each";
import kristal from "../../assets/Img/Osilator/kristal.png";
import kristalrangkaian from "../../assets/Img/Osilator/kristalrangkaian.png";
import module from "../../assets/Img/Modulator/mod1.png"
import module2 from "../../assets/Img/Modulator/mod2.png"
import { Image } from "antd";

function Modulator() {
  return [
    {
      title: "Pendahuluan",
      content: (
        <div>
      
          <p>
            Pemodulasian yang dipakai pada pesawat latih pemancar/penerima AM ini
            adalah pemodulasian tingkat tinggi, karena rangkaian ini yang umum
            dipakai. Pada sistem yang kita bahas ini pemodulasian dinjeksikan pada
            penguat daya RF (power RF amplifier) melalui trafo kopling yang lazim
            dinamakan trafo modulasi.
          </p>
          <p>
            Untuk dapat memahami modulasi AM, secara sederhana dapat diartikan
            bahwa proses ini adalah menjadikan amplitudo sinyal RF terdefinisi
            oleh amplitudo sinyal informasi yang ditumpangkannya.
          </p>
          <p>
            Secara ringkas dapat diterangkan bahwa prinsip modulator adalah
            sebuah trafo keluaran yang difungsikan sebagal sarana untuk
            menginjeksikan sinyal audio (informasi) ke dalam sinyal pembawa
            (carrier) pada rangkaian pemancar. Pada sistem ini sinyal informasi
            tersebut dinjeksikan pada penguat daya melalui kolektor transistor
            penguat daya.
          </p>
          <p>
            Pemasangan trafo pemodulasi (modulator) dan penguat sinyal pemodulasi
            adalah seperti pada Gambar 5.2;
          </p>
          <div className="w-full justify-center flex flex-col my-4">
            <Image src={module} alt="Gambar 5.2 Rangkaian Modulator" />
            <p className="text-center text-sm">Gambar 5.2 Rangkaian Modulator</p>
          </div>
          <p>Proses pemodulasian tersebut menghasilkan bentuk gelombang AM seperti berikut:</p>
          <div className="w-full justify-center flex flex-col my-4">
            <Image src={module2} alt="Gambar 5.3 Gelombang Modulasi AM" />
            <p className="text-center text-sm">Gambar 5.3 Gelombang Modulasi AM</p>
          </div>
          <p>
            <strong>Keterangan:</strong>
          </p>
          <p>
            Vc adalah amplitudo frekuensi pembawa.
            <br />
            Vm adalah amplitudo frekuensi sinyal modulasi.
            <br />
            Vc = 3.579,545 kHz dan Fm=1kHz.
          </p>
          <p>
            <strong>Penentuan indeks modulasi (m) adalah:</strong>
          </p>
          <p>
            Hubungkan keluaran generator fungsi dengan masukan "MIC" modulator.
            Atur generator fungsi agar menghasilkan gelombang sinusoidal dengan
            frekuensi 1kHz dengan tegangan sekitar 50mVp-p. Jika tidak tersedia
            generator fungsi, dapat menggunakan sinyal 1kHz yang dibangkitkan oleh
            sumber sinyal yang ada di dalam rangkaian modulator tersebut dengan
            cara mengubah posisi saklar (switch) pada panel Modulator dari posisi
            "MIC" menjadi "INT".
          </p>
        </div>
      ),
    },
    {
      title: "Step 1",
      content: (
        <p>
          Periksa kembali hubungan semua peralatan. Jika sudah benar, nyalakan catu-daya.
        </p>
      ),
    },
    {
      title: "Step 2",
      content: (
        <p>
          Dengan menggunakan osiloskop amati bentuk gelombang masukan dan gelombang keluaran.
        </p>
      ),
    },
    {
      title: "Step 3",
      content: (
        <p>
          Gambarlah bentuk gelombang masukan dan gelombang keluaran tersebut.
        </p>
      ),
    },
    {
      title: "Step 4",
      content: (
        <p>
          Tentukan besar tegangan gelombang masukan dan gelombang keluaran tersebut dengan menggunakan osiloskop.
        </p>
      ),
    },
    {
      title: "Step 5",
      content: (
        <p>
          Tentukan besar penguatan tegangan antara tegangan masukan dan tegangan keluaran.
        </p>
      ),
    },
    {
      title: "Step 6",
      content: (
        <p>
          Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3 dan dummy load.
        </p>
      ),
    },
    {
      title: "Step 7",
      content: (
        <p>
          Aktifkan fault simulator 1 panel Power RF. Indikator "1" menyala.
        </p>
      ),
    },
    {
      title: "Step 8",
      content: (
        <p>
          Amati, ukur dan gambarkan bentuk gelombang pada TP1, TP2, TP3, dan dummy load.
        </p>
      ),
    },
    {
      title: "Step 9",
      content: (
        <p>
          Aktifkan fault simulator 2 panel power RF. Indikator "2" menyala.
        </p>
      ),
    },
    {
      title: "Step 10",
      content: (
        <p>
          Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, dan dummy load.
        </p>
      ),
    },
    {
      title: "Step 11",
      content: (
        <p>
          Aktifkan Fault Simulator 3 panel power RF. Indikator "3" menyala.
        </p>
      ),
    },
    {
      title: "Step 12",
      content: (
        <p>
          Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, dan dummy load.
        </p>
      ),
    },
    {
      title: "Step 13",
      content: (
        <p>
          Aktifkan ketiga Fault Simulator dan Power RF. Indikator "1", "2" dan "3" menyala.
        </p>
      ),
    },
    {
      title: "Step 14",
      content: (
        <p>
          Amati, ukur, dan gambarkan bentuk gelombang pada TP1, TP2, TP3, dan dummy load.
        </p>
      ),
    },
    {
      title: "Kesimpulan",
      content: (
        <div>
          <p>
            1. Dari percobaan di atas diperoleh pengukuran, pengamatan, dan penganalisisan bentuk-bentuk gelombang pada sistem modulasi AM.
          </p>
          <p>
            2. Pengamatan gelombang AM dengan berbagai derajat indeks modulasi dapat dilaksanakan dengan mengubah-ubah amplitudo sinyal generator atau dengan mengubah posisi tombol Volume pada panel Modulator.
          </p>
        </div>
      ),
    },
  ];
}

export default Modulator;

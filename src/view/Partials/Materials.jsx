import React from "react";

function Materials() {
  return (
    <div className="px-2 md:px-6 my-3 w-full text-slate-700 dark:text-white flex flex-col items-center">
      <div className="text-left flex flex-col w-full items-center justify-center rounded-xl">
        <div className="w-full">
          <div
            className="bg-gradient-to-r from-blue-400 via-blue-300 to-blue-200 dark:bg-slate-800 rounded-xl w-full flex flex-col md:flex-row justify-start shadow-lg transition-all duration-300 transform hover:scale-105"
          >
            <div className="w-full p-6">
              <div>
                <h2 className="text-blue-600 text-center dark:text-blue-400 font-bold uppercase text-lg md:text-xl">
                  Pemancar (Transmitter)
                </h2>
                <p className="text-justify indent-8 text-blue-900 dark:text-white text-sm md:text-lg xl:text-xl mt-4 font-medium">
                  Pemancar (Transmitter) adalah sebuah alat untuk memancarkan
                  suatu sinyal. Cara kerja pemancar secara sederhana yaitu
                  menguatkan dan memodulasikan sinyal masukan lalu dialirkan ke
                  antenna untuk di ubah menjadi gelombang elektromaknetik.
                  Parameter dari sebuah pemancar berupa : Rentan frekuensi,
                  daya, efesiensi, dan derau. Struktur dasar transmisi berupa :
                  Masukan- Modulator- PLL- preamplifier- penguat daya- antena
                  pemancar.
                </p>
                <p className="text-justify indent-8 text-blue-900 dark:text-white text-sm md:text-lg xl:text-xl mt-4 font-medium">
                  Terdapat dua jenis modulasi yaitu : Modulasi analog dan
                  modulasi digital. Modulasi analog yakni modulasi yang merespon
                  sinyal analog. Teknik umum yang dipakai dalam modulasi analog
                  adalah : Modulasi sudut dan modulasi amplitudo. Modulasi
                  digital merupakan proses penumpangan sinyal digital ke sinyal
                  pembawa. Terdapat tiga prinsip dasar modulasi digital, yaitu :
                  Amplitude Shift Keying (ASK), Frequency Shift Keying (FSK),
                  dan Phase Shift Keying (PSK).
                </p>
                <p className="text-justify indent-8 text-blue-900 dark:text-white text-sm md:text-lg xl:text-xl mt-4 font-medium">
                  Modulator berfungsi untuk memodulasikan frekuensi sinyal
                  masukan menjadi frekuensi sinyal yang tepat. Mixer bertugas
                  untuk mentransformasikan beberapa frekuensi. PLL (Phase Lock
                  Loop) dipakai untuk menghasilkan frekuensi sinyal keluaran
                  yang diinginkan dari frekuensi sinyal masukan. Pre-Amplifier
                  berfungsi sebagai penguat sinyal sebelum masuk attenuator.
                  Attenuator merupakan komponen RF yang melemahkan level sinyal.
                  Penguat daya adalah penguat akhir sebelum dipancarkan lewat
                  antena.
                </p>
              </div>
              <p className="flex text-left text-xs md:text-sm text-blue-700 dark:text-blue-400 font-bold leading-normal items-center mt-4">
                <svg
                  height="21"
                  viewBox="0 0 21 21"
                  width="21"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    transform="translate(6 6)"
                  >
                    <path d="m8.5 7.5v-7h-7"></path>
                    <path d="m8.5.5-8 8"></path>
                  </g>
                </svg>
                {/* News Site */}
              </p>
            </div>
          </div>
        </div>
        <div hidden className="lg:px-4 w-full mt-6">
          <div className="flex justify-center">
            <div className="bg-white dark:bg-slate-800 rounded-xl shadow-lg flex w-full lg:w-1/2 p-4 justify-between items-center">
              <button
                className="border-slate-500 dark:border-slate-100 hover:text-blue-500 dark:hover:text-blue-400 border hover:border-blue-500 dark:hover:border-blue-400 w-10 h-10 rounded-full duration-150 hover:scale-105 active:scale-95 flex items-center justify-center"
                onClick={() => console.log("Rotate Left")}
              >
                <svg
                  height="22"
                  viewBox="0 0 21 21"
                  width="22"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    transform="translate(2 2)"
                  >
                    <path d="m9.55 11.4-3-2.9 3-3"></path>
                  </g>
                </svg>
              </button>
              <button
                className="bg-blue-400 h-10 w-10 transition-all duration-150 hover:scale-110 active:scale-95 ease-in-out rounded-full shadow flex items-center justify-center"
                onClick={() => console.log("Center Button Clicked")}
              ></button>
              <button
                className="border-slate-500 dark:border-slate-100 hover:text-blue-500 dark:hover:text-blue-400 border hover:border-blue-500 dark:hover:border-blue-400 w-10 h-10 rounded-full duration-150 hover:scale-105 active:scale-95 flex items-center justify-center"
                onClick={() => console.log("Rotate Right")}
              >
                <svg
                  height="22"
                  viewBox="0 0 21 21"
                  width="22"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    transform="translate(2 2)"
                  >
                    <path d="m7.5 11.5 3-3-3.068-3"></path>
                  </g>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Materials;

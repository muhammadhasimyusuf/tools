import React, { lazy } from "react";
import TNU from "../../assets/Img/TNU.png";
import { Button, Divider, Modal, Typography } from "antd";
import { useState } from "react";
import Each from "../../Component/Each";
import { BiLeftArrow, BiRightArrow } from "react-icons/bi";
import "../../assets/css/Styles.css";
import Frequency from "../Partials/Frequency";
import Power from "../Partials/Power";
import { useDispatch, useSelector } from "react-redux";
import { changeCurrent, setDataStep } from "../Partials/StepSlice";
import Materials from "../Partials/Materials";
import StepsRender from "../Partials/StepsRender";
import Driver from "../Partials/Driver";
import Buffers from "../Partials/Buffer";
import Osilator from "../Partials/Osilator";
import PowerSupply from "../Partials/Power";
import OsilatorCristal from "../Partials/OsilatorCristal";
import Modulator from "../Partials/Modulator";
import SelfTyping from "../../Component/SelfTyping";

const { Title } = Typography;

function HomePage() {
  const [dataModalOpen, setdataModalOpen] = useState(false);
  const [materialsModalOpen, setmaterialsModalOpen] = useState(false);
  const [typeRender, settypeRender] = useState([]);
  const dispatch = useDispatch();

  const lazyComponent = {
    frequency: Frequency,
    power: Power,
    voltage: StepsRender,
    Driver,
    Buffers,
    Osilator,
    PowerSupply,
    // OsilatorCristal,
    Modulator
  };

  const Render = lazyComponent[typeRender];
  const current = useSelector((state) => state.StepSlice.current);
  const stepData = useSelector((state) => state.StepSlice.data);

  const handleModal = async (e, type, data) => {
    e.stopPropagation();
    e.preventDefault();
    if (type === "method") {
      settypeRender("Buffers");
      dispatch(setDataStep(lazyComponent["Buffers"]()));

      dispatch(changeCurrent(0));

      setTimeout(() => {
        setdataModalOpen(true);
      }, 0);
    }
    if (type === "materials") {
      setmaterialsModalOpen(true);
    }
    if (type === "type") {
      dispatch(changeCurrent(0));
      settypeRender(data);
      dispatch(setDataStep(lazyComponent[data]()));
    }
    if (type === "next") {
      current != stepData.length - 1 && dispatch(changeCurrent(current + 1));
    }
    if (type === "prev") {
      current != 0 && dispatch(changeCurrent(current - 1));
    }
  };
  return (
    <>
      <div className="flex flex-col md:flex-row h-screen  max-h-[55vh] m-7 space-y-4 md:space-y-0 md:space-x-4">
        <div className="flex-1 border-r md:border-r-0 md:border-b h-full flex flex-col justify-center items-center p-5 bg-gradient-to-r from-blue-100 to-indigo-100 rounded-xl shadow-lg">
        <Title className="relative text-center text-indigo-800 text-3xl font-extrabold">
            <span
              className="relative z-10 text-3xl cursor-none bg-gradient-to-r from-purple-400 via-pink-500 to-red-500 text-white p-3 rounded-lg shadow-2xl transform transition duration-500 hover:scale-110 hover:shadow-pink-500/50"
              style={{ fontFamily: "Pacifico, cursive" }}
            >
              Selamat Datang
            </span>
            <span className="absolute left-1/2 transform -translate-x-1/2 bottom-0 h-1 w-1/2 bg-gradient-to-r from-purple-400 via-pink-500 to-red-500 transition-all duration-500 hover:h-2 rounded-full"></span>
          </Title>
          <img
            className="rounded-full h-48 w-48 md:h-full md:w-full object-cover shadow-md"
            src={TNU}
            alt="Aplikasi Media Pembelajaran"
          />
          <Title className="relative text-center text-indigo-800 text-3xl font-extrabold">
            <span
              // className="relative z-10 text-2xl bg-gradient-to-r from-purple-400 via-pink-500 to-red-500 text-white p-3 rounded-lg shadow-2xl transform transition duration-500 hover:scale-110 hover:shadow-pink-500/50"
              style={{ fontFamily: "Pacifico, cursive" }}
            >
              <SelfTyping
                strings={[  { text: "Aplikasi", className: "relative z-10 text-2xl bg-gradient-to-r from-blue-400 via-blue-300 to-blue-200 text-white p-3 rounded-lg shadow-2xl transform transition duration-500 hover:scale-110 hover:shadow-blue-300/50"},
                { text: "Media", className: "relative z-10 text-2xl bg-gradient-to-r from-blue-400 via-blue-300 to-blue-200 text-white p-3 rounded-lg shadow-2xl transform transition duration-500 hover:scale-110 hover:shadow-blue-300/50" },
                { text: "Pembelajaran", className: "relative z-10 text-2xl bg-gradient-to-r from-blue-400 via-blue-300 to-blue-200 text-white p-3 rounded-lg shadow-2xl transform transition duration-500 hover:scale-110 hover:shadow-blue-300/50" }]}
              />
            </span>
            {/* <span className="absolute left-1/2 transform -translate-x-1/2 bottom-0 h-1 w-1/2 bg-gradient-to-r from-purple-400 via-pink-500 to-red-500 transition-all duration-500 hover:h-2 rounded-full"></span> */}
          </Title>
        </div>

        <div className="flex-1 h-full self-center bg-white rounded-xl shadow-md">
          <div className="p-6 sm:p-16 h-full flex flex-col justify-center">
            <div className="grid gap-4">
              <Button
                onClick={(e) => handleModal(e, "method")}
                className="group h-12 px-6 border-2 border-gray-300 rounded-full transition duration-300 hover:border-blue-400 focus:bg-blue-50 active:bg-blue-100"
              >
                <div className="relative hover:animate-bounce flex items-center space-x-4 justify-center">
                  <span className="block w-max font-semibold tracking-wide text-gray-700 text-sm transition duration-300 group-hover:text-blue-600 sm:text-base">
                    Cara Pengukuran
                  </span>
                </div>
              </Button>
              <Button
                onClick={(e) => handleModal(e, "materials")}
                className="group h-12 px-6 border-2 border-gray-300 rounded-full transition duration-300 hover:border-blue-400 focus:bg-blue-50 active:bg-blue-100"
              >
                <div className="relative hover:animate-bounce flex items-center space-x-4 justify-center">
                  <span className="block w-max font-semibold tracking-wide text-gray-700 text-sm transition duration-300 group-hover:text-blue-600 sm:text-base">
                    Materi
                  </span>
                </div>
              </Button>
            </div>
          </div>
        </div>
      </div>

      <Modal
        title={[
          <div
            key="modal-title"
            className="w-full flex tabs justify-between border-b py-1 text-sm font-bold sticky top-0 bg-white z-10"
          >
            <Each
              of={[
                { label: "Penyangga", data: "Buffers" },
                { label: "Osilator", data: "Osilator" },
                { label: "Driver", data: "Driver" },
                { label: "Modulator", data: "Modulator" },
                { label: "Power Supply", data: "PowerSupply" },
                // { label: "Oscillator X'tal", data: "OsilatorCristal" },
              ]}
              render={(item) => (
                <div
                  key={item.data}
                  onClick={(e) => handleModal(e, "type", item.data)}
                  className={`px-7 text-center w-full py-1 text-sm font-bold hover:cursor-pointer ${
                    typeRender === item.data
                      ? "bg-blue-100 rounded-md shadow-inner"
                      : ""
                  }`}
                >
                  {item.label}
                </div>
              )}
            />
          </div>,
        ]}
        centered
        open={dataModalOpen}
        onOk={() => setdataModalOpen(false)}
        onCancel={() => setdataModalOpen(false)}
        className="!w-full max-w-7xl"
        footer={null}
      >
        <div className="h-screen max-h-[50vh] min-h-80 px-7 overflow-y-auto">
          {dataModalOpen && <StepsRender key={typeRender} />}
        </div>
        <div className="border-t flex justify-center py-2">
          <Button
            onClick={(e) => handleModal(e, "prev")}
            className="border-none"
            size="small"
            icon={<BiLeftArrow />}
          />
          <Button
            onClick={(e) => handleModal(e, "next")}
            className="border-none"
            size="small"
            icon={<BiRightArrow />}
          />
        </div>
      </Modal>

      <Modal
        title={[
          <div key="materials-title" className="w-full border-b border-black">
            Materi
          </div>,
        ]}
        centered
        open={materialsModalOpen}
        onOk={() => setmaterialsModalOpen(false)}
        onCancel={() => setmaterialsModalOpen(false)}
        className="!w-full h-screen max-w-7xl"
        footer={null}
      >
        <Materials />
      </Modal>
    </>
  );
}

export default HomePage;

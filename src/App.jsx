import { useState } from 'react'
import './App.css'
import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Route, Routes } from 'react-router-dom'
import HomePage from './view/Home/HomePage'
import Cointainer from './view/Main/Cointainer';


function App() {
  return (
    <Cointainer>
     <Routes>
      <Route exact path="/" element={<HomePage />} />
     </Routes>
     
    </Cointainer>
  )
}

export default App

import { combineReducers } from "@reduxjs/toolkit"
import StepSlice from "../view/Partials/StepSlice"

const rootReducers = combineReducers({
 StepSlice
})

export default rootReducers

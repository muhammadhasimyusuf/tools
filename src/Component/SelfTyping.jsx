import React, { useState, useEffect, memo } from 'react';

const SelfTyping = ({ strings, typingSpeed = 100, deletingSpeed = 50, pauseDuration = 1000 }) => {
  const [displayText, setDisplayText] = useState('');
  const [currentStringIndex, setCurrentStringIndex] = useState(0);
  const [charIndex, setCharIndex] = useState(0);
  const [isDeleting, setIsDeleting] = useState(false);

  useEffect(() => {
    if (!isDeleting && charIndex < strings[currentStringIndex].text.length) {
      const timeout = setTimeout(() => {
        setDisplayText((prev) => prev + strings[currentStringIndex].text[charIndex]);
        setCharIndex((prev) => prev + 1);
      }, typingSpeed);
      return () => clearTimeout(timeout);
    } else if (!isDeleting && charIndex === strings[currentStringIndex].text.length) {
      const timeout = setTimeout(() => {
        setIsDeleting(true);
      }, pauseDuration);
      return () => clearTimeout(timeout);
    } else if (isDeleting && charIndex > 0) {
      const timeout = setTimeout(() => {
        setDisplayText((prev) => prev.slice(0, -1));
        setCharIndex((prev) => prev - 1);
      }, deletingSpeed);
      return () => clearTimeout(timeout);
    } else if (isDeleting && charIndex === 0) {
      setIsDeleting(false);
      setCurrentStringIndex((prev) => (prev + 1) % strings.length);
    }
  }, [charIndex, isDeleting, strings, currentStringIndex, typingSpeed, deletingSpeed, pauseDuration]);

  return (
    <div className="self-typing">
      <style>
        {
          `
          .cursor {
            display: inline-block;
            margin-left: 1px;
            opacity: 1;
            animation: blink 1s step-end infinite;
          }

          @keyframes blink {
            0%, 100% { opacity: 1; }
            50% { opacity: 0; }
          }

          `
        }
      </style>
      <span className={`${strings[currentStringIndex].className}`}>{displayText}</span>
      <span className="cursor">|</span>
    </div>
  );
};

export default memo(SelfTyping);

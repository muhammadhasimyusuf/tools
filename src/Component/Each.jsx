import React, { Children } from 'react'
import PropTypes from 'prop-types';

const Each = ({render, of, keyIndex}) =>
Children.toArray(of?.map((item, index, self) => render(item, keyIndex ? item[keyIndex] : index, self, keyIndex ? index : undefined)))

Each.propTypes = {
    of : PropTypes.array,
    keyIndex : PropTypes.string
}

export default Each